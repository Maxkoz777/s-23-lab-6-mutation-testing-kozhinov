import pytest

from bonus_system import calculateBonuses


def test_calculate_bonuses_standard_program():
    assert calculateBonuses('Standard', 0) == pytest.approx(0.5)
    assert calculateBonuses('Standard', 1) == pytest.approx(0.5)
    assert calculateBonuses('Standard', 10000) == pytest.approx(0.75)
    assert calculateBonuses('Standard', 10001) == pytest.approx(0.75)
    assert calculateBonuses('Standard', 50000) == pytest.approx(1)
    assert calculateBonuses('Standard', 50001) == pytest.approx(1)
    assert calculateBonuses('Standard', 100000) == pytest.approx(1.25)
    assert calculateBonuses('Standard', 100001) == pytest.approx(1.25)


def test_calculate_bonuses_premium_programs():
    assert calculateBonuses('Premium', 0) == pytest.approx(0.1)
    assert calculateBonuses('Premium', 1) == pytest.approx(0.1)
    assert calculateBonuses('Premium', 10000) == pytest.approx(0.15)
    assert calculateBonuses('Premium', 10001) == pytest.approx(0.15)
    assert calculateBonuses('Premium', 50000) == pytest.approx(0.2)
    assert calculateBonuses('Premium', 50001) == pytest.approx(0.2)
    assert calculateBonuses('Premium', 100000) == pytest.approx(0.25)
    assert calculateBonuses('Premium', 100001) == pytest.approx(0.25)


def test_calculate_bonuses_diamond_programs():
    assert calculateBonuses('Diamond', 0) == pytest.approx(0.2)
    assert calculateBonuses('Diamond', 1) == pytest.approx(0.2)
    assert calculateBonuses('Diamond', 10000) == pytest.approx(0.3)
    assert calculateBonuses('Diamond', 10001) == pytest.approx(0.3)
    assert calculateBonuses('Diamond', 50000) == pytest.approx(0.4)
    assert calculateBonuses('Diamond', 50001) == pytest.approx(0.4)
    assert calculateBonuses('Diamond', 100000) == pytest.approx(0.5)
    assert calculateBonuses('Diamond', 100001) == pytest.approx(0.5)


def test_calculate_bonuses_invalid_program():
    assert calculateBonuses('Invalid', 0) == pytest.approx(0)
    assert calculateBonuses('    ', 1) == pytest.approx(0)
    assert calculateBonuses('Invalid', 10000) == pytest.approx(0)
    assert calculateBonuses('AAAA', 10001) == pytest.approx(0)
    assert calculateBonuses('Invalid', 50000) == pytest.approx(0)
    assert calculateBonuses('XXXX', 50001) == pytest.approx(0)
    assert calculateBonuses('Invalid', 100000) == pytest.approx(0)
    assert calculateBonuses('ZZZZ', 100001) == pytest.approx(0)
